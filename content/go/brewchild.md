+++
title = "Brewchild"
description = "Client for the brewfather API"
author = "Till Klocke"
date = "2021-05-01T12:47:13+02:00"
publishdate = "2021-05-01T12:47:13+02:00"
tags = ["Development", "Go", "Brewfather"]
categories = ["Bastelei"]
comments = false
removeBlur = false
toc = false
packages = ["stackoverflow.beer/go/brewchild"]
repo = "https://codeberg.org/StackOverflowBrewery/go-brewchild"
+++
