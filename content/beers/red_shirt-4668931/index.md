---
title: Red Shirt
description: Named after the famous Red Shirts of Star Trek TOS this beer is our entry
    to the Maisel and friends homebrew competition 2022. Brewed with Honey Buscuit
    and Salty Caramel malts from the Swaen this beer focuses on the malty aspects
    of this style and leaves the hops as a supporting actor.
abv: 5
ibu: 32
og: 13.569511475874943
fg: 0
buGuRation: 0.58
beerColor: 15.8
untappdId: "4668931"
malts:
  - name: Pilsen Malt
    supplier: BestMalz
    amount: 0.185
  - name: Red X
    supplier: BestMalz
    amount: 1.132
  - name: Smoked
    supplier: BestMalz
    amount: 0.069
  - name: BlackSwaen Honey Biscuit
    supplier: The Swaen
    amount: 0.164
  - name: Platinumswaen Salty Caramel
    supplier: The Swaen
    amount: 0.164
hops:
  - name: Columbus (Tomahawk)
    origin: US
    amount: 7.5
    usage: Bittering
    alpha: 14
  - name: Willamette
    origin: US
    amount: 7.5
    usage: Aroma
    alpha: 5.5
miscs: []
yeasts:
  - name: Empire Ale Yeast
    laboratory: Mangrove Jack's
    productId: M15
lastBrewDate: "2021-11-27T23:00:00Z"
lastBatch: 21
author: StackOverflow Brewery
date: 2021-11-27T23:00:00Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
