---
title: Kurbellager
description: Dry, crisp, fruity and crushable. The right beer to end a day full cycling
    if you are pretty sure you don't need to get on your bike again today.
abv: 6.4
ibu: 22
og: 13.569511475874943
fg: 0
buGuRation: 0.4
beerColor: 6
untappdId: "4428828"
malts:
  - name: Pilsen Malt
    supplier: BestMalz
    amount: 4.93
  - name: Smoked
    supplier: BestMalz
    amount: 0.19
  - name: Vienna
    supplier: BestMalz
    amount: 0.79
  - name: Carared
    supplier: Weyermann
    amount: 0.45
hops:
  - name: Amarillo
    origin: US
    amount: 118.3
    usage: Aroma
    alpha: 8.7
  - name: Galaxy
    origin: Australia
    amount: 36.6
    usage: Bittering
    alpha: 15
miscs: []
yeasts:
  - name: Saflager Lager
    laboratory: Fermentis
    productId: W-34/70
lastBrewDate: "2021-07-11T07:54:37Z"
lastBatch: 17
author: StackOverflow Brewery
date: 2021-07-11T07:54:37Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
