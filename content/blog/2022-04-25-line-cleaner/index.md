+++
title = "Bierleitungsreiniger für faule"
description = "Bierleitungen gründlich und günstig mit einer Pumpe spülen"
author = "Till Klocke"
date = "2022-04-25T17:54:48+01:00"
publishdate = "2022-04-25T17:54:48+01:00"
tags = ["Bastelei", "Anleitung", "Duotight", "HowTo"]
categories = ["Anleitung"]
comments = false
removeBlur = false
toc = false
draft = false
[[images]]
  src = "/blog/2022-04-25-line-cleaner/reiniger.jpg"
  alt = "Leitungsreiniger mit NC-Anschluss und kleiner Pumpe"
  stretch = "c"
+++

Als Hobbybrauer*innen leiten wir unser leckeres Bier durch so einiges an Schläuchen. Angefangen
von der Kochpfanne in den Fermenter bis hin zum Schlauch am Zapfhahn. Viele dieser Schläuche, vor 
allem die zum Brauprozess zugehörigen, sind häufig bereits Teil eines guten Reinigungsprogramms.
Aber was ist mit den ganzen Schläuchen zum Umdrücken, Schläuchen an Zwickelhähnen und natürlich den
Schläuchen am Kegerator? Da gibt es viele verschiedene Möglichkeiten. Einige dieser Möglichkeiten
sind unkomfortabel oder sie kosten CO2.

Wir haben daher einen kleinen Leitungsreiniger auf Basis einer kleine Springbrunnenpumpe gebastelt.
{{< img "pumpe.jpg" "Kleine schwarze Spingbrunnenpumpe" >}}
Diese kleinen Pumpen laufen in der Regel mit 12V und sind oft im Baumarkt oder bei den üblichen
Onlineshops für wenige Euro erhätlich. Um einen solchen Leitungsreiniger zu bauen werden nur
wenige Teile benötigt:

* kleine Springbrunnenpumpe 12V mit einen Schlauchnippel von ca. 6-8mm Durchmesser
* ein Stück 9,5mm Duotight-kompatibler Schlauch (EVABarrier o.ä., Länge je nach Wunsch)
* ein kleines Stück 8mm Duotight-kompatibler Schlauch
* ein Duotight-Adapter von 8mm auf 9,5mm
* ein Duotight-Adapter von 8mm auf 6,5mm
* eine Carbonation Cap aus Kunststoff (kein Edelstahl)
* kleines 12V-Netzteil (z.B. von einem alten Router o.ä.)
* Schrumpfschlauch 2 verschiedene Größen

Der Aufbau ist recht simpel. Die Carbonation Caps aus Kunststoff haben eine Duotight-kompatible
Tülle. Hier wird der 6,5mm auf 8mm Adapter aufgesteckt (mit der 6,5mm-Seite an die Carbonation Cap)
{{< img "cap.jpg" "Carbonation Cap über zwei Adapter an 9,5mm Schlauch angeschlossen" >}}
Dann folgt ein kleines Stückchen 8mm Schlauch und an diesen kommt dann der 8mm auf 9,5mm Adapter. Und
schlussendlich der 9,5mm Schlauch. Der Aufbau ist so gewählt, damit der 9,5mm Schlauch den größten Teil
der Schlauchstrecke ausmacht und damit weniger Widerstand bietet als ein 8mm Schlauch. Außerdem lässt sich
der 9,5mm Schlauch leichter über die Schlauchnippel an den Pumpen ziehen.

Am Ende wird der 9,5mm Schlauch nur noch über den Schlauchnippel an der Pumpe gezogen. Keine Sorge, falls
dieser ein wenig zu groß sein sollte. Der 9,5mm Schlauch kann einfach für ein paar Sekunden in kochendes 
Wasser gehalten werden und wird dadurch weicher und elastischer. Ist der Schlauch entsprechend aufgewärmt 
kann er einfach und vorsichtig (ohne sich zu verbrühen) über den Schlauchnippel gezogen werden. Am Schluss
haben wir den Schlauch noch mit einem Kabelbinder gesichert, aber das ist eher optional.

Um die Pumpe mit Strom zu versorgen wird ein kleines Netzteil benötigt, was 12V DC liefert. Diese gibt
es günstig in diversen Onlineshops oder es liegt vielleicht bereits ein altes Netzteil von einem
Router oder so herum. Einfach den Stecker abschneiden und die beiden innenliegende Kabel abisolieren.
Nun die kleineren Schmrumpfschläuche über jeweils ein innenliegendes Kabel schieben und den größeren
Schrumpfschlauch über des ganze Kabel. Jeweils den Plus-Pol des Netzteils mit dem positiven Anschluss
der Pumpe verlöten und den Schrumpfschlauch über die blanken Kabel ziehen und schrumpfen lassen, und das 
gleiche am Minus-Pol. Am Schluss den Großen Schrumpfschlauch über die Verbindungsstelle ziehen und schrumpfen
lassen. Durch die ganzen Schrumpfschläuche wird die Verbindungsstelle zwar vermutlich nicht wasserdicht,
aber wasserresistent genug, dass Spritzwasser kein Problem sein sollte.

Um Schläuche zu reinigen kann nun einfach ein Schlauch mit NC-Anschluss an der Carbonation Cap angeschlossen, 
die Pumpe in einen Behälter mit Reinigungsflüssigkeit getaucht (z.B. warmes PBW) und das ganze
eingeschaltet werden. Das andere Ende des zu reinigenden Schlauchs sollte natürlich wieder zurück
in den Behälter führen, damit die Reinigungsflüssigkeit im Kreis gepumpt werden kann.

Das ganze ist eine recht nützliche Konstruktion, die auch an den Kegerator angeschlossen werden kann, wobei
der Behälter mit der Reinigungsflüssigkeit dann z.B. unter dem Zapfhahn steht. An Nukatap-Zapfhähnen
kann aber auch ein NC-Anschluss befestigt werden, wodurch andere Schläuche gleich mitgereinigt werden können.

Natürlich ersetzt diese Methode nicht das regelmäßige Austauschen von Schläuchen und das Zerlegen und
gründliche Reinigen von NC-Anschlüssen. Es zögert beides jedoch weiter hinaus und spart dadurch ein wenig Arbeit.
